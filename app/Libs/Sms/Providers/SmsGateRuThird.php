<?php

namespace App\Libs\Sms\Providers;

class SmsGateRuThird extends SmsGate
{
    public function __construct()
    {
        $this->gate_prefix = 'RU_THIRD';
    }
}