<?php

namespace App\Libs\Sms\Providers;

class SmsGateFi extends SmsGate
{
    public function __construct()
    {
        $this->gate_prefix = 'FI';
    }
}