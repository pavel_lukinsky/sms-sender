<?php

namespace App\Libs\Sms\Providers;

class SmsGateRuFirst extends SmsGate
{
    public function __construct()
    {
        $this->gate_prefix = 'RU_FIRST';
    }
}