<?php

namespace App\Libs\Sms\Providers;

class SmsGateEe extends SmsGate
{
    public function __construct()
    {
        $this->gate_prefix = 'EE';
    }
}