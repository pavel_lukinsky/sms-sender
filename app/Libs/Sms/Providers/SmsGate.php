<?php 

namespace App\Libs\Sms\Providers;

use App\Libs\Sms\Contracts\SmsGateInterface;

class SmsGate implements SmsGateInterface 
{
    /**
     * log file path. 
     * 
     * @var string
     */
    const LOG_PATH = '/storage/logs/smssend.log';
    
    /**
     * prefix for gate identification
     * 
     * @var string
     */
    protected $gate_prefix;
    
    /**
     * For test will send in log file
     * 
     * {@inheritDoc}
     * @see \App\Libs\Sms\Contracts\SmsGateInterface::send()
     */
    public function send($phone, $text): void
    {
        $message = 'gate: ' . $this->gate_prefix . ' phone: ' . $phone . ' text: ' . $text . PHP_EOL;
        
        file_put_contents(app()->basePath() . self::LOG_PATH, $message, FILE_APPEND);
    }
}