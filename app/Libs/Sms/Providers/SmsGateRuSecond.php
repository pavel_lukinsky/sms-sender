<?php

namespace App\Libs\Sms\Providers;

class SmsGateRuSecond extends SmsGate
{
    public function __construct()
    {
        $this->gate_prefix = 'RU_SECOND';
    }
}