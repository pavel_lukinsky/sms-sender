<?php

namespace App\Libs\Sms\Providers;

class SmsGateBy extends SmsGate
{
    public function __construct()
    {
        $this->gate_prefix = 'BY';
    }
}