<?php 

namespace App\Libs\Sms\Contracts;

interface SmsGateInterface 
{    
    /**
     * Send message to phone
     * 
     * @param string $phone
     * @param string $text
     * @return bool
     */
    public function send($phone, $text): void;
}