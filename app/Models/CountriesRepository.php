<?php

namespace App\Models;

use App\Libs\Sms\Providers\SmsGate;
use App\Libs\Sms\Providers\SmsGateBy;
use App\Libs\Sms\Providers\SmsGateEe;
use App\Libs\Sms\Providers\SmsGateFi;
use App\Libs\Sms\Providers\SmsGateRuFirst;
use App\Libs\Sms\Providers\SmsGateRuSecond;
use App\Libs\Sms\Providers\SmsGateRuThird;

/**
 * Closed class for countries information.
 * By functions it is both a repository and a service.
 * In a real application, it would be best to separate them.
 */
class CountriesRepository 
{
    /**
     * Gate action type (send|dispatch)
     * @var string
     */
    public $gate_action;
    
    /**
     * Gate for send or dispatch
     * 
     * @var SmsGate
     */
    public $gate;
    
    /**
     * Current code of phone in work
     * @var int
     */
    private $code;
    
    /**
     * Countries information
     * 
     * key of array phone code of country
     * 
     * @var array
     */
    private $info = [
        358 => [
            'name' => 'Финляндия',
            'gates' => [
                0 => SmsGateFi::class
            ]
        ],
        372 => [
            'name' => 'Эстония',
            'gates' => [
                0 => SmsGateEe::class
            ]
        ],
        375 => [
            'name' => 'Белоруссия',
            'gates' => [
                0 => SmsGateBy::class
            ]
        ],
        7 => [
            'name' => 'Россия',
            'gates' => [
                0 => SmsGateRuFirst::class,
                1 => SmsGateRuSecond::class,
                2 => SmsGateRuThird::class
            ]
        ]
    ];
        
    /**
     * Gates capacity
     * 
     * @var array
     */
    private $capacity = [];
    
    /**
     * Setting gate info by phone
     * 
     * @param string $phone
     * @return void
     */
    public function setGateInfo(string $phone): void
    {        
        if (!$this->getCountryInfo($phone)) {
            $this->gate_action = 'error';
            return;
        }
        
        $this->gate_action = 'send';
        
        if (count($this->info[$this->code]['gates']) === 1) {
            $this->gate = new $this->info[$this->code]['gates'][0];
            
            return;
        }
        
        $this->gate_action = 'dispatch';
        
        $this->distributeGateLoad();
    }
    
    /**
     * Return country information by phone code
     * 
     * @param string $phone
     * @return boolean
     */
    private function getCountryInfo($phone): bool
    {
        $phone = ltrim($phone, '+');
        
        foreach ($this->info as $code => $country_info) {
            if ($code != substr($phone, 0, strlen($code))) continue;
                         
            $this->code = $code;
            
            return true;
        }
        
        return false;
    }
    
    /**
     * Distribute the load on gates
     * 
     * @return SmsGate
     */
    private function distributeGateLoad(): void
    {
        if (!isset($this->capacity[$this->code]) || $this->capacity[$this->code] == count($this->info[$this->code]['gates']) - 1) {
            $this->capacity[$this->code] = 0;
            
            $this->gate = new $this->info[$this->code]['gates'][0];
            
            return;
        }
        
        $gate_key = ++$this->capacity[$this->code];
        
        $this->gate = new $this->info[$this->code]['gates'][$gate_key];
    }
}