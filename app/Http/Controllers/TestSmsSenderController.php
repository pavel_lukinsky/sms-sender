<?php

namespace App\Http\Controllers;

class TestSmsSenderController extends Controller
{
    /**
     * Functional sms sender test
     * 
     * @return void
     */
    public function index(): void
    {        
        $info = [
            'phones' => [
                // russian phones
                '+79500147125',
                '+79500147126',
                '+79500147127',
                '+79500147128',
                '+79500147129',
                '+79500147130',
                '+79500147131',
                '+79500147132',
                '+79500147133',
                '+79500147134',
                '+79500147135',
                '+79500147136',
                '+79500147137',
                '+79500147138',
                '+79500147139',
                '+79500147140',
                '+79500147141',
                '+79500147142',
                '+79500147143',
                '+79500147144',
                '+79500147145',
                '+79500147146',
                // belarus phones
                '+375177523023',
                '+375177523025',
                '+375177523026',
                '+375177523027',
                // finland phones
                '+358952535253',
                '+358952535254',
                '+358952535255',
                '+358952535256',
                '+358952535257',
                // estonia phones
                '+372736563656',
                '+372736563657',
                '+372736563658',
                '+372736563659',
                '+372736563660',
                '+372736563661',
                '+372736563662',
            ],
            'text' => 'Test message'
        ];
        
        $post = json_encode($info);
        
        if ($curl = curl_init(url('/') . '/send-sms')) {
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, false);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
            
            echo curl_exec($curl);
            
            curl_close($curl);
        }
    }
}
