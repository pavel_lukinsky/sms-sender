<?php

namespace App\Http\Controllers;

use App\Jobs\SendSms;
use Illuminate\Http\Request;
use App\Models\CountriesRepository;

class SendSmsController extends Controller
{   
    /**
     * Sending sms
     * 
     * @param Request
     * @return void
     */
    public function index(Request $request): void
    {     
        $phones = $request->json('phones');
        $text = $request->json('text');
        
        $country = new CountriesRepository();
        
        foreach ($phones as $phone) {
            $country->setGateInfo($phone);
            
            if ($country->gate_action === 'error') {
                // loging error
                
                continue;
            }
            
            if ($country->gate_action === 'send') {
                $country->gate->send($phone, $text);
                
                continue;
            }
            
            dispatch(new SendSms($phone, $text, $country->gate));     
        }
    }
}
