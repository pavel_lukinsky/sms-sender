<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Libs\Sms\Providers\SmsGate;

class SendSms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    /**
     * Phone to send message
     * 
     * @var string
     */
    private $phone;
    
    /**
     * Sending text
     * 
     * @var string
     */
    private $text;
    
    private $gate;

    /**
     * Create a new job instance.
     *
     * @param string $phone
     * @param string $text
     * @return void
     */
    public function __construct(string $phone, string $text, SmsGate $gate)
    {
        $this->phone = $phone;
        $this->text = $text;
        $this->gate = $gate;
    }

    /**
     * Execute the job.
     *
     * @param SmsGate $gate
     * @return void
     */
    public function handle(): void
    {
        $this->gate->send($this->phone, $this->text);
    }
}
